<?php
/**
 * *
 *  * 
 *  * See LICENSE.md bundled with this module for license details.
 *
 */
namespace FrancySolutions\ILabImporter\Console\Command\Customer;
use Magento\ImportExport\Model\Import;
use FrancySolutions\ILabImporter\Console\Command\AbstractExportCommand;
/**
 * Class TestCommand
 * @package FireGento\FastSimpleImport2\Console\Command
 *
 */
class ExportCustomer extends AbstractExportCommand
{


    protected function configure()
    {
        $this->setName('ilabimporter:customers:export')
            ->setDescription('Export Customers');
        $this->setEntityCode('customer');

        parent::configure();
    }


}