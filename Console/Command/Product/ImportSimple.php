<?php

/**
 * 
 * See LICENSE.md bundled with this module for license details.
 */

namespace FrancySolutions\ILabImporter\Console\Command\Product;

use FrancySolutions\ILabImporter\Console\Command\AbstractImportCommand;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\ImportExport\Model\Import;
use Magento\Catalog\Api\ProductAttributeOptionManagementInterface;
use PDO;

/**
 * Class TestCommand
 * @package FireGento\FastSimpleImport2\Console\Command
 *
 */
class ImportSimple extends AbstractImportCommand {

    /**
     * @var ProductAttributeOptionManagementInterface
     */
    private $attributeOptionManagementInterface;
    private $host = 'x.x.x.x';
    private $username = "xxxx";
    private $password = "xxxx";
    private $database = "xxxx";
    private $products = array();
    private $limit = 100;
    private $connection;

    public function deleteOption($attrCode, $label) {
        $attributeOptionList = $this->attributeOptionManagementInterface->getItems($attrCode);
        foreach ($attributeOptionList as $attributeOptionInterface) {
            if ($attributeOptionInterface->getLabel() == $label) {
                $this->attributeOptionManagementInterface->delete($attrCode, $attributeOptionInterface->getValue());
            }
        }
    }

    protected function configure() {
        $this->setName('ilabimporter:products:importsimple')
                ->setDescription('Import Simple Products ');
        $this->setBehavior(Import::BEHAVIOR_ADD_UPDATE);
        $this->setEntityCode('catalog_product');

        parent::configure();
    }

    protected function setupConnection() {
        try {
            $this->connection = new PDO("sqlsrv:server={$this->host} ; Database={$this->database}", $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die(print_r($e->getMessage()));
        }
    }

    protected function fetchProducts($params = array()) {
        $tsql = "SELECT TOP {$this->limit} * FROM CARILLO.dbo.AAVX_MAGENTO_ARTICOLI WHERE MG64_DESCRMARCA = 'ARTIGLI'";
        $getProducts = $this->connection->prepare($tsql);
        $getProducts->execute($params);
        $products = $getProducts->fetchAll(PDO::FETCH_ASSOC);
        $productCount = count($products);

        if ($productCount > 0) {
            $this->products = $products;
        } else {
            die("Nothing to import");
        }
    }

    /**
     * @return array
     */
    protected function getEntities() {
        $this->setupConnection();
        $this->fetchProducts();
        $this->attributeOptionManagementInterface = $this->objectManager->get("Magento\Catalog\Api\ProductAttributeOptionManagementInterface");
        $data = [];
        $grouped_products = array();
        if (sizeof($this->products)) {
            foreach ($this->products as $product) {
                $grouped_products[$product['MG66_CODART']][] = $product;
            }
            foreach ($grouped_products as $key => $configureable_products) {
                $parent_product = array();
                $configurable_variations = array();
                $count = 0;
                foreach ($configureable_products as $product_variation) {
                    $image_name = trim($product_variation['MG65_CODBARCODE']) . ".png";
                    if ($count == 0) {
                        $count++;
                        $parent_product = array(
                            'sku' => trim($product_variation['MG65_CODBARCODE']),
                            'attribute_set_code' => 'Default',
                            'product_type' => 'configurable',
                            'product_websites' => 'base',
                            'name' => trim($product_variation['DescrizioneNoComposizione']),
                            'ean' => $product_variation['MG65_CODBARCODE'],
                            'visibility' => 'Catalog, Search',
                            'tax_class_name' => 'Taxable Goods',
                            'short_description' => NULL,
                            'description' => trim($product_variation['MG87_DESCART']),
                            'configurable_variation_labels' => 'Size'
                        );
                        if (file_exists("pub/media/import/" . $image_name)) {
                            $parent_product['base_image'] = trim($product_variation['MG65_CODBARCODE']) . ".png";
                            $parent_product['thumbnail_image'] = trim($product_variation['MG65_CODBARCODE']) . ".png";
                            $parent_product['product_online'] = 1;
                        } else {
                            $parent_product['product_online'] = 0;
                        }
			if (strlen(trim($product_variation['MadeIn']))) {
                            $parent_product['country_of_manufacture'] = trim($product_variation['MadeIn']);
                        }
                        if (strlen(trim($product_variation['MG53_DESCRFAM']))) {
                            $parent_product['categories'] = trim($product_variation['MG53_DESCRFAM']);
                        }
                    } else {
                        $variant = array(
                            'sku' => trim($product_variation['MG65_CODBARCODE']),
                            'attribute_set_code' => 'Default',
                            'product_type' => 'simple',
                            'product_websites' => 'base',
                            'name' => trim($product_variation['DescrizioneNoComposizione']),
                            'ean' => trim($product_variation['MG65_CODBARCODE']),
                            'visibility' => 'Catalog, Search',
                            'tax_class_name' => 'Taxable Goods',
                            'short_description' => NULL,
                            'description' => trim($product_variation['MG87_DESCART'])
                        );
                        if (file_exists("pub/media/import/" . $image_name)) {
                            $variant['base_image'] = trim($product_variation['MG65_CODBARCODE']) . ".png";
                            $variant['thumbnail_image'] = trim($product_variation['MG65_CODBARCODE']) . ".png";
                            $variant['product_online'] = 1;
                        } else {
                            $variant['product_online'] = 0;
                        }
                        $var = array();
                        $var['sku'] = $product_variation['MG65_CODBARCODE'];
                        if (strlen(trim($product_variation['MG70_QGIACATT']))) {
                            $variant['qty'] = trim($product_variation['MG70_QGIACATT']);
                        } else {
                            $variant['qty'] = 0;
                        }
                        if (strlen(trim($product_variation['LI10_PREZZO']))) {
                            $variant['price'] = trim($product_variation['LI10_PREZZO']);
                        } else {
                            $variant['price'] = "0.00";
                        }
                        if (trim($product_variation['CodiceTaglia'])) {
                            $variant['size'] = trim($product_variation['CodiceTaglia']);
                            $var['size'] = trim($product_variation['CodiceTaglia']);
                        }
                        if (trim($product_variation['CodiceColore']) !== "-") {
                            $variant['color'] = trim($product_variation['CodiceColore']);
                            $var['color'] = trim($product_variation['CodiceColore']);
                        }
                        if (strlen(trim($product_variation['MadeIn']))) {
                            $variant['country_of_manufacture'] = trim($product_variation['MadeIn']);
                        }
                        if (strlen(trim($product_variation['MG53_DESCRFAM']))) {
                            $variant['categories'] = trim($product_variation['MG53_DESCRFAM']);
                        }
                        $configurable_variations[] = $var;
                        $data[] = $variant;
                    }
                }
                $parent_product['configurable_variations'] = $configurable_variations;
                $data[] = $parent_product;
            }
        }
        return $data;
    }

}
