<?php
/**
 * 
 * See LICENSE.md bundled with this module for license details.
 */
namespace FrancySolutions\ILabImporter\Console\Command\Product;
use FrancySolutions\ILabImporter\Console\Command\AbstractImportCommand;
use Magento\ImportExport\Model\Import;
use PDO;

/**
 * Class TestCommand
 * @package FireGento\FastSimpleImport2\Console\Command
 *
 */
class DeleteAll extends AbstractImportCommand
{

    private $host = "";
    private $username = "";
    private $password = "";
    private $database = "";
    private $products = array();
    
    protected function setupConnection() {
           try {
               $this->connection = new PDO("sqlsrv:server={$this->host} ; Database={$this->database}", $this->username, $this->password);
               $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           } catch (Exception $e) {
               die(print_r($e->getMessage()));
           }
    }
    protected function configure()
    {
        $this->setName('ilabimporter:products:deleteall')
            ->setDescription('Delete Products ');

        $this->setBehavior(Import::BEHAVIOR_DELETE);
        $this->setEntityCode('catalog_product');

        parent::configure();
    }
    
    protected function fetchProducts($params = array()) {
        $tsql = "SELECT DISTINCT MG65_CODBARCODE FROM CARILLO.dbo.AAVX_MAGENTO_ARTICOLI WHERE MG64_DESCRMARCA = 'ARTIGLI'";
        $getProducts = $this->connection->prepare($tsql);
        $getProducts->execute($params);
        $products = $getProducts->fetchAll(PDO::FETCH_COLUMN, 0);
        $productCount = count($products);

        if ($productCount > 0) {
            $this->products = $products;
        }
    }
    /**
     * @return array
     */
    protected function getEntities()
    {
        $this->setupConnection();
        $this->fetchProducts();
        $productFactory = $this->objectManager->create('Magento\Catalog\Model\ProductFactory');
        $productCollection  = $productFactory->create()->getCollection();
        $data = [];
        if(!empty($this->products)) {
            foreach($productCollection as $product) {
                if(!in_array($product->getSku(),$this->products)) {
                    $data[] = array(
                        'sku' => $product->getSku()
                    );
                }
            }
        }
        return $data;
    }
}