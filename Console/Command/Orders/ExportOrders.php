<?php
/**
 * *
 *  * 
 *  * See LICENSE.md bundled with this module for license details.
 *
 */
namespace FrancySolutions\ILabImporter\Console\Command\Orders;
use Magento\ImportExport\Model\Import;
use FrancySolutions\ILabImporter\Console\Command\AbstractExportCommand;
/**
 * Class TestCommand
 * @package FireGento\FastSimpleImport2\Console\Command
 *
 */
class ExportOrders extends AbstractExportCommand
{


    protected function configure()
    {
        $this->setName('ilabimporter:orders:export')
            ->setDescription('Export Orders ');
        $this->setEntityCode('order');

        parent::configure();
    }


}