<?php
/**
 * *
 *  * 
 *  * See LICENSE.md bundled with this module for license details.
 *
 */
namespace FrancySolutions\ILabImporter\Console\Command;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\State;
use Magento\ImportExport\Model\Import;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PDO;

/**
 * Class TestCommand
 * @package FireGento\FastSimpleImport2\Console\Command
 *
 */
abstract class AbstractExportCommand extends Command
{

    private $host = '';
    private $username = '';
    private $password = "";
    private $database = "";
    /**
     * @var string
     */
    protected $behavior;
    /**
     * @var string
     */
    protected $entityCode;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * Object manager factory
     *
     * @var ObjectManagerFactory
     */
    private $objectManagerFactory;

    /**
     * Constructor
     *
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(ObjectManagerFactory $objectManagerFactory)
    {
        $this->objectManagerFactory = $objectManagerFactory;
        parent::__construct();
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return null|int null or 0 if everything went fine, or an error code
     */
    protected function setupConnection() {
        try {
            $this->connection = new PDO("sqlsrv:server={$this->host} ; Database={$this->database}", $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die(print_r($e->getMessage()));
        }
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $omParams = $_SERVER;
        $omParams[StoreManager::PARAM_RUN_CODE] = 'admin';
        $omParams[Store::CUSTOM_ENTRY_POINT_PARAM] = true;
        $this->objectManager = $this->objectManagerFactory->create($omParams);

        $area = FrontNameResolver::AREA_CODE;

        /** @var \Magento\Framework\App\State $appState */
        $appState = $this->objectManager->get('Magento\Framework\App\State');
        $appState->setAreaCode($area);
        $configLoader = $this->objectManager->get('Magento\Framework\ObjectManager\ConfigLoaderInterface');
        $this->objectManager->configure($configLoader->load($area));

        $output->writeln('Export started');

        $time = microtime(true);
     
        $this->setupConnection();
        $orders = $this->objectManager->create('Magento\Sales\Model\Order')->getCollection();
        foreach($orders as $order)
        {
            $order_id = $order->getIncrementId();
            $query = "select count(ID) from AA_ORDERS where ID = ?";
            $stmt = $this->connection->prepare( $query, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );  
            $stmt->execute(array($order_id));
            if($stmt->fetchColumn() == 0) {
                $query = "insert into AA_ORDERS(ID, Company,FirstName,LastName,Address1,Address2,City,ZipCode,Country,VatNumber,FiscalCode,Notes,Shipping_FirstName,Shipping_LastName,Shipping_Address1,Shipping_Address2,Shipping_City,Shipping_ZipCode,Shipping_Country,Shipping_Method,Payment_Method,Total) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $billingAddress = $order->getBillingAddress();
                $shippingAddress = $order->getShippingAddress();
                $params = [];
                $params["ID"] = $order->getIncrementId();
                $params["Company"] = "";
                $params["FirstName"] = $order->getData('customer_firstname');
                $params["LastName"]  = $order->getData('customer_firstname');
                $params["Address1"] = $billingAddress->getStreet(1)[0];
                $params["Address2"] = $billingAddress->getStreet(2)[0];
                $params["City"] = $billingAddress->getCity();
                $params["ZipCode"] = $billingAddress->getPostcode();
                $params["Country"] = $billingAddress->getCountry();
                $params["VatNumber"] = $billingAddress->getTaxvat();
                $params["FiscalCode"] = $billingAddress->getTaxvat();
                $params["Notes"] = "";
                $params["Shipping_FirstName"] = $shippingAddress->getFirstname();
                $params["Shipping_LastName"] = $shippingAddress->getLastname();
                $params["Shipping_Address1"] = $shippingAddress->getStreet(1)[0];
                $params["Shipping_Address2"] = $shippingAddress->getStreet(2)[0];
                $params["Shipping_City"] = $shippingAddress->getCity();
                $params["Shipping_ZipCode"] = $shippingAddress->getPostcode();
                $params["Shipping_Country"] = $shippingAddress->getCountry();
                $params["Shipping_Method"] = $order->getShippingMethod();
                $params["Payment_Method"] = $order->getPayment()->getMethod();
                $params["Total"] = $order->getGrandTotal();

                $output->writeln('Saving order ' . $params["ID"]   . "\n");

                $stmt = $this->connection->prepare( $query, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );  
                $stmt->execute(array_values($params)); 
                $orderItems = $order->getAllItems();
                foreach($orderItems as $item)
                {
                    $query = "insert into AA_ORDER_ITEMS(Id,Fk_Order,Sku,Title,Description,Quantity,Price) values(?,?,?,?,?,?,?)";
                    $item_params = [];
                    $item_params["Id"] = $order->getIncrementId();
                    $item_params["Fk_Order"] = $params["ID"];
                    $item_params["Sku"] = $item->getSku();
                    $item_params["Title"] = $item->getName();
                    $item_params["Description"] = "";
                    $item_params["Quantity"] = intval($item->getQtyOrdered());
                    $item_params["Price"] = $item->getPrice();
                    $stmt = $this->connection->prepare( $query, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 1  ) );  
                    $stmt->execute(array_values($item_params));
                    $output->writeln('Saving item ' . $item_params["Sku"] . ' for order ID ' .$item_params["Fk_Order"]  . "\n");

                }
            }else{
                    $output->writeln('Skipping order '  . $order_id . "\n");
                
            }
        }

        $output->writeln('Export finished. Elapsed time: ' . round(microtime(true) - $time, 2) . 's' . "\n");
        $this->afterFinishImport();

    }



    /**
     * @return string
     */
    public function getEntityCode()
    {
        return $this->entityCode;
    }

    /**
     * @param string $entityCode
     */
    public function setEntityCode($entityCode)
    {
        $this->entityCode = $entityCode;
    }


    public function afterFinishImport(){

    }

}